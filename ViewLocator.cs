using System;
using Avalonia.Controls;
using Avalonia.Controls.Templates;
using RickRoll.ViewModels;

namespace RickRoll
{
    public class ViewLocator : IDataTemplate
    {
        private const string ViewModelPostfix = "ViewModel";
        private const string ViewPostfix = "View";

        public Control? Build(object? data)
        {
            if (data is null)
            {
                return null;
            }

            var type = data.GetType();
            var name = GetViewName(type);

            if (type != null)
            {
                var control = (Control)Activator.CreateInstance(type)!;
                control.DataContext = data;
                return control;
            }

            return new TextBlock { Text = $"Not Found: {name}" };
        }

        public bool Match(object? data)
        {
            return data is ViewModelBase;
        }

        private string GetViewName(Type type)
        {
            var name = type.FullName!;
            var lastIndex = name.LastIndexOf(ViewModelPostfix, StringComparison.Ordinal);

            if (lastIndex < 0)
            {
                return name;
            }

            return name.Substring(0, lastIndex) + ViewPostfix;
        }
    }
}
