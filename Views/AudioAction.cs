using NAudio.Wave;
using System;
using System.Threading.Tasks;

namespace RickRoll.Views
{
    public class AudioAction
    {
        private readonly WaveOutEvent waveOut;
        private AudioFileReader audioFileReader;
        private bool isPlaying;
        private readonly Action onAudioPlaybackFinished;

        public AudioAction(Action onAudioPlaybackFinished)
        {
            waveOut = new WaveOutEvent();
            this.onAudioPlaybackFinished = onAudioPlaybackFinished;
        }

        public void LoadAudio(string path)
        {
            string audioFilePath = path;

            if (waveOut.PlaybackState == PlaybackState.Playing)
            {
                waveOut.Stop();
            }

            audioFileReader = new AudioFileReader(audioFilePath);
            waveOut.Init(audioFileReader);
        }

        public async Task PlayAudio()
        {
            if (isPlaying)
            {
                return;
            }

            waveOut.Play();
            isPlaying = true;

            await Task.Delay((int)audioFileReader.TotalTime.TotalMilliseconds);

            waveOut.Stop();
            isPlaying = false;
            onAudioPlaybackFinished.Invoke();
        }
    }
}