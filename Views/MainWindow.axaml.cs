using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media.Imaging;
using Avalonia.Threading;
using System;
using System.Collections.Generic;

namespace RickRoll.Views
{
    public partial class MainWindow : Window
    {
        private HashSet<Key> PressedKeys { get; } = new();
        private DispatcherTimer? timer;
        private SpriteMove spriteMove;
        private AudioAction audioAction;

        public MainWindow()
        {
            InitializeComponent();

            spriteMove = new SpriteMove();
            audioAction = new AudioAction(ResetImage);

            spriteMove.OnDown += () => HandleRickAction(RickRollAction.Down);
            spriteMove.OnUp += () => HandleRickAction(RickRollAction.Up);
            spriteMove.OnLeft += () => HandleRickAction(RickRollAction.Left);
            spriteMove.OnRight += () => HandleRickAction(RickRollAction.Right);

            KeyDown += spriteMove.HandleKeyDown;
            KeyUp += HandleKeyUp;
        }

private void HandleRickAction(RickRollAction action)
{
    audioAction.LoadAudio("../../../Assets/Sounds/RickRoll.Wav");

    switch (action)
    {
        case RickRollAction.Down:
            Character.Source = new Bitmap("../../../Assets/Images/RickRollDown.png");
            Canvas.SetTop(Character, 0);
            break;
        case RickRollAction.Up:
            Character.Source = new Bitmap("../../../Assets/Images/RickRollUp.png");
            Canvas.SetTop(Character, 0);
            break;
        case RickRollAction.Left:
            Character.Source = new Bitmap("../../../Assets/Images/RickRollLeft.png");
            Canvas.SetTop(Character, 0);
            break;
        case RickRollAction.Right:
            Character.Source = new Bitmap("../../../Assets/Images/RickRollRight.png");
            Canvas.SetTop(Character, 0);
            break;
    }

    audioAction.PlayAudio();
}

        private void HandleKeyUp(object? sender, KeyEventArgs e)
        {
            PressedKeys.Remove(e.Key);
            timer?.Stop();
        }

        private void ResetImage()
        {
            Character.Source = new Bitmap("../../../Assets/Images/RickRollDefault.png");
            Canvas.SetTop(Character, 0);
        }
    }

    enum RickRollAction
    {
        Up,
        Down,
        Left,
        Right
    }
}