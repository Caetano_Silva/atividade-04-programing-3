using Avalonia.Input;
using System;

namespace RickRoll.Views
{
    public class SpriteMove

    {
        public delegate void MoveAction();

        public MoveAction? OnDown { get; set; }
        public MoveAction? OnUp { get; set; }
        public MoveAction? OnLeft { get; set; }
        public MoveAction? OnRight { get; set; }

        public void HandleKeyDown(object? sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Down:
                case Key.S:
                    OnDown?.Invoke();
                    break;
                case Key.Up:
                case Key.W:
                    OnUp?.Invoke();
                    break;
                case Key.Left:
                case Key.A:
                    OnLeft?.Invoke();
                    break;
                case Key.Right:
                case Key.D:
                    OnRight?.Invoke();
                    break;
            }
        }
    }
}
